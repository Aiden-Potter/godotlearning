extends Node2D

signal health_depleted
signal health_changed(old_value, new_value)

var health = 10

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func take_damage(amount):
	var old_health = health
	health -= amount
	health_changed.emit(old_health, health)
	if health <= 0:
		health_depleted.emit()
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
